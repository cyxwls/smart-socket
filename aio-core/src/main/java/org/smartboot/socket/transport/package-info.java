/**
 * 提供AIO通信服务的具体实现。
 * @author 三刀
 * @version V1.0 , 2018/5/19
 */
package org.smartboot.socket.transport;